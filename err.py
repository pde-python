#!/usr/bin/python
#-*- coding: utf-8 -*-

from optparse import OptionParser

import numpy as np

import const as c
from grid import ExplicitSolver, norm
from waves import Exm


def err_main():
    '''
    Calculates error (norm) series
    '''
    
    # parser initialization
    parser = OptionParser()
    parser.add_option('--hy', dest='hy', default='1e-7')
    parser.add_option('--hz', dest='hz', default='1e-7')
    parser.add_option('--ht', dest='ht', default='1e-16')
    (opts, args) = parser.parse_args()

    # constants
    time = np.double('1e-14')
    ly = np.double('2e-5')
    lz = np.double('2e-5')
    
    ht = np.double(opts.ht)
    hy = np.double(opts.hy)
    hz = np.double(opts.hz)

    solver = ExplicitSolver(int(ly / hy), int(lz / hz), time, ht, solver='inline')
    uh = solver.solve()

    u = uh.copy()
    M = 550
    for i in xrange(u.shape[0]):
        for j in xrange(u.shape[1]):
            u[i, j] = Exm(i * hy, j * hz, time, M)

    print norm(u, uh)
    

if __name__ == '__main__':
    err_main()
