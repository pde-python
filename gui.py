#-*- coding: utf-8

# Политика префиксов в именах
# QLineEdit - edit_
# QLabel - label_
# QPushButton - button_


import sys, os

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure

import numpy as np

from grid import ExplicitSolver, ImplicitSolver, norm
try:
    from waves import Exm, setParameters
except ImportError:
    from third import Exm, setParameters
import const as co


ABOUT_IMAGE = 'about.jpg'
#IMAGE_WIDTH = 500
#IMAGE_HEIGHT = 500

ABOUT_TEXT = u'Курсовую работу выполнили:<br />студенты группы 6407<br /><b>Каранашев Мухамед</b><br /><b>Гайдель Андрей</b>'
ABOUT_TEXT_WIDTH = 300


def split_number(x):
    return ('%g' % x).split('e')

def normal(x):
    return np.float64('1e' + split_number(x)[1])

class Dummy(object):
    pass

class AppForm(QMainWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle(u'Курсовой проект')

        self.data = Dummy()
        self.data.counter_sol = self.data.counter_res = 1

        self.create_main_frame()
        self.setWindowState(Qt.WindowMaximized)

        self.on_grid_change()

        # self.test_graph()

    def create_main_frame(self):
        self.main_frame = QWidget()
        self.init_work_frame()
        self.init_param_frame()

        self.splitter = QSplitter()
        self.splitter.addWidget(self.param_frame)
        
        self.splitter.addWidget(self.work_frame)
        self.main_layout = QGridLayout(self.main_frame)
        # self.main_layout.addWidget(self.param_frame, 0, 0)
        # self.main_layout.addWidget(self.work_frame, 0, 1)
        self.main_layout.addWidget(self.splitter)
        self.setCentralWidget(self.main_frame)

    def init_param_frame(self):
        self.param_frame = QWidget()
        param_group = QGroupBox(u"&Параметры")
        param_layout = QGridLayout(self.param_frame)
        param_layout.addWidget(param_group, 0, 0)

        self.edit_LY = QLineEdit(str(co.LY))
        self.edit_LY.setMinimumWidth(40)
        self.edit_LZ = QLineEdit(str(co.LZ))
        self.edit_LZ.setMinimumWidth(40)
        self.edit_C = QLineEdit(str(co.C))
        self.edit_C.setMinimumWidth(40)
        self.edit_LAMBDA = QLineEdit(str(co.LAMBDA))
        self.edit_LAMBDA.setMinimumWidth(40)
        self.edit_T = QLineEdit('1e-14')
        self.edit_dim_y = QLineEdit(str(50))
        self.edit_dim_z = QLineEdit(str(50))
        
        pg_layout = QFormLayout(param_group)
        pg_layout.addRow(u'Ширина ly', self.edit_LY)
        pg_layout.addRow(u'Высота lz', self.edit_LZ)
        pg_layout.addRow(u'Скорость C', self.edit_C)
        pg_layout.addRow(u'Волна λ', self.edit_LAMBDA)
        pg_layout.addRow(u'Момент T', self.edit_T)
        pg_layout.addRow(u'Размер по y (I)', self.edit_dim_y)
        pg_layout.addRow(u'Размер по z (J)', self.edit_dim_z)
        self.param_frame.setMaximumWidth(250)

        self.connect(self.edit_LY, SIGNAL("returnPressed()"), self.on_grid_change)
        self.connect(self.edit_LZ, SIGNAL("returnPressed()"), self.on_grid_change)
        self.connect(self.edit_dim_y, SIGNAL("returnPressed()"), self.on_grid_change)
        self.connect(self.edit_dim_z, SIGNAL("returnPressed()"), self.on_grid_change)

        bu_group = QWidget()
        bu_layout = QVBoxLayout(bu_group)
        save_solution = QPushButton("Save solution")
        save_research = QPushButton("Save research")
        self.connect(save_solution, SIGNAL("clicked()"), self.save_solution)
        self.connect(save_research, SIGNAL("clicked()"), self.save_research)
        bu_layout.addWidget(save_solution)
        bu_layout.addWidget(save_research)
        param_layout.addWidget(bu_group, 3, 0)

    def save_solution(self):
        base = os.path.join('graph', 'solution', str(self.data.counter_sol))
        self.z_fig.savefig(base + '.png')
        print 'saved', base + '.png'
        self.z_fig.savefig(base + '.eps')
        print 'saved', base + '.eps'
        self.data.counter_sol += 1

    def save_research(self):
        base = os.path.join('graph', 'research', str(self.data.counter_res))
        self.n_fig.savefig(path + '.png')
        print 'saved', base + '.png'
        self.n_fig.savegif(path + '.eps')
        print 'saved', base + '.eps'
        self.data.counter_res += 1

    def _make_analytic(self):
        a_group = QGroupBox(u"Аналитическое решение")
        a_group.setCheckable(True)
        a_layout = QFormLayout(a_group)
        
        self.edit_length = QLineEdit(str(500))
        a_layout.addRow(u"Элементов ряда", self.edit_length)
        self.edit_length.setMaximumWidth(100)
        
        return a_group

    def _make_explicit(self):
        e_group = QGroupBox(u"Явная схема")
        e_group.setCheckable(True)
        e_group.setChecked(False)
        e_layout = QFormLayout(e_group)
        self.edit_exp_step_t = QLineEdit()
        e_layout.addRow(u"Шаг по t", self.edit_exp_step_t)
        
        return e_group

    def _make_implicit(self):
        i_group = QGroupBox(u"Неявная схема")
        i_group.setCheckable(True)
        i_group.setChecked(False)
        i_layout = QFormLayout(i_group)
        self.edit_imp_step_t = QLineEdit()
        i_layout.addRow(u"Шаг по t", self.edit_imp_step_t)
        
        return i_group

    def _make_t_norm(self):
        t_group = QGroupBox(u'Ось T')
        t_layout = QFormLayout(t_group)

        self.edit_k_start = QLineEdit(str(10))
        self.edit_k_end = QLineEdit(str(100))
        self.edit_k_step = QLineEdit(str(10))
        self.edit_k_M = QLineEdit(str(600))
        self.t_norm_button = QPushButton(u'Пуск')
        self.connect(self.t_norm_button, SIGNAL("clicked()"), self.on_test_ht)
        t_layout.addRow(u'Мин. узлов', self.edit_k_start)
        t_layout.addRow(u'Макс. узлов', self.edit_k_end)
        t_layout.addRow(u'Шаг', self.edit_k_step)
        t_layout.addRow(u'Ряд', self.edit_k_M)
        t_layout.addRow(self.t_norm_button)

        return t_group
        

    def _make_y_norm(self):
        y_group = QGroupBox(u"Ось Y")
        y_layout = QFormLayout(y_group)

        self.edit_y_start = QLineEdit('10')
        self.edit_y_end = QLineEdit('100')
        self.edit_y_step = QLineEdit('10')
        self.edit_y_ht = QLineEdit('1e-16')
        self.edit_y_M = QLineEdit(str(600))
        self.y_norm_button = QPushButton(u'Пуск')
        self.connect(self.y_norm_button, SIGNAL("clicked()"), self.on_test_hy)
        y_layout.addRow(u'Мин. узлов', self.edit_y_start)
        y_layout.addRow(u'Макс. узлов', self.edit_y_end)
        y_layout.addRow(u'Шаг', self.edit_y_step)
        y_layout.addRow(u'ht', self.edit_y_ht)
        y_layout.addRow(u'Ряд', self.edit_y_M)
        y_layout.addRow(self.y_norm_button)
        
        return y_group

    def _make_z_norm(self):
        z_group = QGroupBox(u"Ось Z")
        z_layout = QFormLayout(z_group)

        self.edit_z_start = QLineEdit('10')
        self.edit_z_end = QLineEdit('100')
        self.edit_z_step = QLineEdit('10')
        self.edit_z_ht = QLineEdit('1e-16')
        self.edit_z_M = QLineEdit(str(600))
        self.z_norm_button = QPushButton(u'Пуск')
        self.connect(self.z_norm_button, SIGNAL("clicked()"), self.on_test_hz)
        z_layout.addRow(u'Мин. узлов', self.edit_z_start)
        z_layout.addRow(u'Макс. узлов', self.edit_z_end)
        z_layout.addRow(u'Шаг', self.edit_z_step)
        z_layout.addRow(u'ht', self.edit_z_ht)
        z_layout.addRow(u'Ряд', self.edit_z_M)
        z_layout.addRow(self.z_norm_button)
        
        return z_group

    def _set_solver(self, t):
        if t:
            self.Solver = ExplicitSolver
        else:
            self.Solver = ImplicitSolver
        print self.Solver

    def _make_norm_frame(self):
        self.norm_frame = QWidget()
        self.n_fig = Figure((5.0, 5.0), dpi = self.dpi)
        self.n_canvas = FigureCanvas(self.n_fig)
        self.n_canvas.setParent(self.main_frame)
        self.n_axes = self.n_fig.add_axes([.1, .1, .8, .8])
        n_layout = QGridLayout(self.norm_frame)
        n_layout.addWidget(self.n_canvas, 0, 0)
        n_toolbar = NavigationToolbar(self.n_canvas, self.norm_frame)
        n_layout.addWidget(n_toolbar, 1, 0)

        scheme_type_frame = QWidget()
        scheme_type_layout = QGridLayout(scheme_type_frame)
        self.exp_radio_button = QRadioButton(u'Явная схема', scheme_type_frame)
        self.imp_radio_button = QRadioButton(u'Неявная схема', scheme_type_frame)
        self.connect(self.exp_radio_button, SIGNAL("toggled(bool)"), self._set_solver)
        self.exp_radio_button.setChecked(True)
        scheme_type_layout.addWidget(self.exp_radio_button, 0, 0)
        scheme_type_layout.addWidget(self.imp_radio_button, 0, 1)

        n_layout.addWidget(scheme_type_frame, 2, 0)

        n_param_frame = QWidget()
        n_param_layout = QGridLayout(n_param_frame)
        n_param_layout.addWidget(self._make_t_norm(), 0, 0)
        n_param_layout.addWidget(self._make_y_norm(), 0, 1)
        n_param_layout.addWidget(self._make_z_norm(), 0, 2)
        n_layout.addWidget(n_param_frame, 3, 0)

    def _make_about_frame(self):
        about_frame = QWidget()
        lay = QHBoxLayout(about_frame)
        about_image = QImage(ABOUT_IMAGE)
        image_label = QLabel()
        image_label.setPixmap(QPixmap.fromImage(about_image))
        about_label = QLabel(ABOUT_TEXT)
        about_label.setMinimumWidth(ABOUT_TEXT_WIDTH)
        lay.addWidget(image_label)
        lay.addWidget(about_label)

        return about_frame
    
    def init_work_frame(self):
        self.work_frame = QTabWidget()

        self.dpi = 75
        self.z_fig = Figure((5.0, 5.0), dpi = self.dpi)
        self.z_canvas = FigureCanvas(self.z_fig)
        self.z_canvas.setParent(self.main_frame)
        self.z_axes = self.z_fig.add_axes([.1, .1, .8, .8])

        self.graph_frame = QWidget()
        graph_layout = QGridLayout(self.graph_frame)
        graph_layout.addWidget(self.z_canvas, 0, 0, 1, 1)

        self.mpl_toolbar = NavigationToolbar(self.z_canvas, self.graph_frame)
        # self.mpl_toolbar.setOrientation(Qt.Vertical)
        graph_layout.addWidget(self.mpl_toolbar, 1, 0, 1, 1)
        
        scheme_frame = QWidget()
        scheme_layout = QGridLayout(scheme_frame)
        self.a_group = self._make_analytic()
        self.exp_group = self._make_explicit()
        self.imp_group = self._make_implicit()
        scheme_layout.addWidget(self.a_group, 0, 0, 1, 1)
        scheme_layout.addWidget(self.exp_group, 0, 1, 1, 1)
        scheme_layout.addWidget(self.imp_group, 0, 2, 1, 1)

        self.button_scheme_run = QPushButton(u'Пуск')
        self.connect(self.button_scheme_run, SIGNAL("clicked()"), self.on_scheme_run)
        self.button_scheme_run.setFocus(Qt.ActiveWindowFocusReason)

        graph_layout.addWidget(scheme_frame, 2, 0)
        graph_layout.addWidget(self.button_scheme_run, 3, 0)
        
        self._make_norm_frame()
        
        self.about_frame = self._make_about_frame()

        self.work_frame.addTab(self.graph_frame, u"&Разностная схема")
        self.work_frame.addTab(self.norm_frame, u"&Исследование аппроксимации")
        self.work_frame.addTab(self.about_frame, u"&О программе")

    def on_test_ht(self):
        print self.Solver
        
        try:
            ly, lz, c, lambda_, t = self._parse_args()
            I, J = self._parse_grid()
            k_min = int(str(self.edit_k_start.text()))
            k_max = int(str(self.edit_k_end.text()))
            k_step = int(str(self.edit_k_step.text()))
            M = int(str(self.edit_k_M.text()))
        except ValueError:
            QMessageBox.critical(
                self,
                u'Ошибка',
                u'Параметры введены в неверном формате.<br />Пожалуйста, исправьте их и повторите.')
            return            

        x_vals = []
        y_vals = []
        hz = lz / J
        hy = ly / I

        if not co.test_exp_stable(ly/I, lz/J, t / k_min):
            warn = QMessageBox.warning(
                self,
                u'Явная схема',
                u'С такими параметрами система может быть неустойчива!<br />Вы уверены, что хотите продолжить?',
                QMessageBox.Yes | QMessageBox.No)
            if (warn == QMessageBox.No):
                return
        
        for k_cur in xrange(k_min, k_max + 1, k_step):
            ht = t / k_cur
            print k_cur, ht
            x_vals.append(ht)
            solver = self.Solver(I, J, t, ht, ly=ly, lz=lz, lambda_=lambda_, C=c)
            u = solver.solve()
            for i in xrange(u.shape[0]):
                for j in xrange(u.shape[1]):
                    u[i, j] -= Exm(i * hy, j * hz, t, M)
            y_vals.append(np.max(np.abs(u)))

        self.n_axes.clear()
        line, = self.n_axes.plot(x_vals, y_vals, '.-')

        self.n_axes.grid(True)
        self.n_axes.set_xlabel(u'ht')
        self.n_axes.set_ylabel(u'err')
        self.n_canvas.draw()

    def on_test_hy(self):
        try:
            ly, lz, c, lambda_, t = self._parse_args()
            I, J = self._parse_grid()
            i_min = int(str(self.edit_y_start.text()))
            i_max = int(str(self.edit_y_end.text()))
            i_step = int(str(self.edit_y_step.text()))
            ht = np.float64(str(self.edit_y_ht.text()))
            M = int(str(self.edit_y_M.text()))
        except ValueError:
            QMessageBox.critical(
                self,
                u'Ошибка',
                u'Параметры введены в неверном формате.<br />Пожалуйста, исправьте их и повторите.')
            return            

        x_vals = []
        y_vals = []
        hz = lz / J

        if not co.test_exp_stable(ly/i_max, lz/J, ht):
            warn = QMessageBox.warning(
                self,
                u'Явная схема',
                u'С такими параметрами система может быть неустойчива!<br />Вы уверены, что хотите продолжить?',
                QMessageBox.Yes | QMessageBox.No)
            if (warn == QMessageBox.No):
                return
        
        for i_cur in xrange(i_min, i_max + 1, i_step):
            hy = ly / i_cur
            print i_cur, hy
            x_vals.append(hy)
            solver = self.Solver(i_cur, J, t, ht, ly=ly, lz=lz, lambda_=lambda_, C=c)
            u = solver.solve()
            for i in xrange(u.shape[0]):
                for j in xrange(u.shape[1]):
                    u[i, j] -= Exm(i * hy, j * hz, t, M)
            y_vals.append(np.max(np.abs(u)))

        self.n_axes.clear()
        line, = self.n_axes.plot(x_vals, y_vals, '.-')

        self.n_axes.grid(True)
        self.n_axes.set_xlabel(u'hy')
        self.n_axes.set_ylabel(u'err')
        self.n_canvas.draw()

    def on_test_hz(self):
        try:
            ly, lz, c, lambda_, t = self._parse_args()
            I, J = self._parse_grid()
            j_min = int(str(self.edit_z_start.text()))
            j_max = int(str(self.edit_z_end.text()))
            j_step = int(str(self.edit_z_step.text()))
            ht = np.float64(str(self.edit_z_ht.text()))
            M = int(str(self.edit_z_M.text()))
        except ValueError:
            QMessageBox.critical(
                self,
                u'Ошибка',
                u'Параметры введены в неверном формате.<br />Пожалуйста, исправьте их и повторите.')
            return            

        x_vals = []
        y_vals = []
        hy = ly / I

        if not co.test_exp_stable(ly/I, lz/j_max, ht):
            warn = QMessageBox.warning(
                self,
                u'Явная схема',
                u'С такими параметрами система может быть неустойчива!<br />Вы уверены, что хотите продолжить?',
                QMessageBox.Yes | QMessageBox.No)
            if (warn == QMessageBox.No):
                return
        
        for j_cur in xrange(j_min, j_max + 1, j_step):
            hz = lz / j_cur
            print j_cur, hz
            x_vals.append(hz)
            solver = self.Solver(I, j_cur, t, ht, ly=ly, lz=lz, lambda_=lambda_, C=c)
            u = solver.solve()
            for i in xrange(u.shape[0]):
                for j in xrange(u.shape[1]):
                    u[i, j] -= Exm(i * hy, j * hz, t, M)
            y_vals.append(np.max(np.abs(u)))

        self.n_axes.clear()
        line, = self.n_axes.plot(x_vals, y_vals, '.-')

        self.n_axes.grid(True)
        self.n_axes.set_xlabel(u'hz')
        self.n_axes.set_ylabel(u'err')
        self.n_canvas.draw()

    def on_grid_change(self):
        ly_str = unicode(self.edit_LY.text())
        lz_str = unicode(self.edit_LZ.text())
        ny_str = unicode(self.edit_dim_y.text())
        nz_str = unicode(self.edit_dim_z.text())

        ly = np.float64(ly_str)
        lz = np.float64(lz_str)
        ny = int(ny_str)
        nz = int(nz_str)
        hy = (ly / ny)
        hz = (lz / nz)
        h_exp = min(hy, hz) / (np.sqrt(2) * co.C)
        h_imp = hy / co.C
        exp_l = ("%g" % h_exp).split('e')
        imp_l = ("%g" % h_imp).split('e')
        ht_exp = '1e' + exp_l[1]
        ht_imp = str(np.trunc(np.float64(imp_l[0]))) + 'e' + imp_l[1]
        self.edit_exp_step_t.setText(ht_exp)
        self.edit_imp_step_t.setText(ht_imp)
        
    def clear_axes(self, axes):
        axes.clear()
        axes.grid(True)
        axes.set_xlabel('z')
        axes.set_ylabel('Ex')
    
    def test_graph(self):
        self.clear_axes(self.z_axes)
        self.z_canvas.draw()

    def _parse_a(self):
        return int(unicode(self.edit_length.text()))

    def _parse_grid(self):
        I = int(unicode(self.edit_dim_y.text()))
        J = int(unicode(self.edit_dim_z.text()))
        return I, J
    
    def _parse_exp(self):
        s = unicode(self.edit_exp_step_t.text()).strip()
        if s == '':
            return None
        return np.float64(s)

    def _parse_imp(self):
        s = unicode(self.edit_imp_step_t.text()).strip()
        if s == '':
            return None
        return np.float64(s)

    def _parse_args(self):
        ly, lz, c, lambda_, t = [np.float64(unicode(p.text()).strip()) \
                                 for p in [self.edit_LY, self.edit_LZ, self.edit_C, self.edit_LAMBDA, self.edit_T]]
        return ly, lz, c, lambda_, t
    
    def on_scheme_run(self):
        is_analytic, is_explicit, is_implicit = [g.isChecked() \
                                                 for g in [self.a_group, self.exp_group, self.imp_group]]

        if not (is_analytic or is_explicit or is_implicit):
            return

        cols = 0
        if is_analytic:
            cols += 1
        if is_explicit:
            cols += 1
        if is_implicit:
            cols += 1
        
        exp_ht, imp_ht = None, None

        try:
            if is_analytic:
                m = self._parse_a()
            if is_explicit or is_implicit:
                exp_ht = self._parse_exp()
                imp_ht = self._parse_imp()
        except ValueError:
            QMessageBox.critical(
                self,
                u'Ошибка',
                u'Параметры введены в неверном формате.<br />Пожалуйста, исправьте их и повторите.')
            return
                    
        try:
            ly, lz, c, lambda_, t = self._parse_args()
            I, J = self._parse_grid()
        except ValueError:
            QMessageBox.critical(
                self,
                u'Ошибка',
                u'Параметры введены в неверном формате.<br />Пожалуйста, исправьте их и повторите.')
            return

        if exp_ht is not None:
            if not co.test_exp_stable(ly/I, lz/J, exp_ht):
                warn = QMessageBox.warning(
                    self,
                    u'Явная схема',
                    u'С такими параметрами система может быть неустойчива!<br />Вы уверены, что хотите продолжить?',
                    QMessageBox.Yes | QMessageBox.No)
                if (warn == QMessageBox.No):
                    return

        if imp_ht is not None:
            if not co.test_imp_stable(ly/I, imp_ht):
                warn = QMessageBox.warning(
                    self,
                    u'Неявная схема',
                    u'С такими параметрами система может быть неустойчива!<br />Вы уверены, что хотите продолжить?',
                    QMessageBox.Yes | QMessageBox.No)
                if (warn == QMessageBox.No):
                    return
        
        self.data.z = np.linspace(0, lz, J + 1)
        
        self.z_axes.clear()
        lines = []
        legs = []        
        if is_analytic:
            setParameters(ly, lz, c, lambda_)
            self.data.E_a = [Exm(ly / 2, z_, t, m) for z_ in self.data.z]
            line, = self.z_axes.plot(self.data.z, self.data.E_a, color='red')
            lines.append(line)
            legs.append('Analytical')
        if is_explicit:
            solver = ExplicitSolver(I, J, t, exp_ht, ly=ly, lz=lz, lambda_=lambda_, C=c)
            u = solver.solve()
            self.data.E_exp = u[u.shape[0] / 2, :]
            line, = self.z_axes.plot(self.data.z, self.data.E_exp, color='blue')
            lines.append(line)
            legs.append('Explicit scheme')
        if is_implicit:
            solver = ImplicitSolver(I, J, t, imp_ht, ly=ly, lz=lz, lambda_=lambda_, C=c)
            u = solver.solve()
            self.data.E_imp = u[u.shape[0] / 2, :]
            line, = self.z_axes.plot(self.data.z, self.data.E_imp, color='green')
            lines.append(line)
            legs.append('Implicit scheme')
        self.z_axes.grid(True)
        self.z_axes.set_xlim(0, max(self.data.z))
        emax = max(self.data.E_a)
        self.z_axes.set_ylim(-1.5 * emax, 1.5 * emax)
        self.z_axes.set_xlabel('z')
        self.z_axes.set_ylabel('Ex')
        self.z_axes.legend(lines, legs, loc='upper center', bbox_to_anchor=(0.5, 1.13), ncol=cols)
        self.z_canvas.draw()
