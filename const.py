#-*- coding: utf-8 -*-

from numpy import sqrt, double, power

C = double('3e8')
LY = double('2e-5')
LZ = double('2e-5')
LAMBDA = double('2e-6')

HY = double(1e-7)
HZ = double(1e-7)

def get_characteristic(x):
    if not isinstance(x, double):
        raise TypeError('argument must be numpy.float64')
    else:
        s = "%g" % x
        l = s.split('e')
        return int(l[1])

def timestep(val):
    # val = step / (C * sqrt(2))
    c = get_characteristic(val)
    return power(10, double(c))
    
HT = timestep(HY)

def test_exp_stable(hy=HY, hz=HZ, ht=HZ, c=C):
    gy = (c**2 * ht**2) / (hy**2)
    gz = (c**2 * ht**2) / (hz**2)
    return (gy + gz < 1)

def test_imp_stable(hy=HY, ht=HT, c=C):
    gy = (c**2 * ht**2) / (hy**2)
    return (gy <= 1)
    
    
    
