#-*- coding: utf-8 -*-

import numpy as np
from scipy import weave
from scipy.weave import blitz, inline, converters
from const import timestep
from sweep import native_sweep, sweep

def norm(lhs, rhs):
    if not (isinstance(lhs, np.ndarray)
            and isinstance(rhs, np.ndarray)):
        raise TypeError("arguments must be arrays")
    
    if lhs.shape != rhs.shape:
        raise ValueError("arrays must be the same shape")

    return np.max(np.abs(lhs - rhs))


class ExplicitSolver(object):
    '''Represents equation solver with time stepper    
    '''

    def __init__(self, I, J, time, ht=None, ly=2e-5, lz=2e-5, lambda_=2e-6, C=3e8, solver='inline', debug=False):

        if solver == 'python':
            self._step = self._python_step
        elif solver == 'blitz':
            self._step = self._blitz_step
        elif solver == 'inline':
            self._step = self._inline_step
        else:
            raise ValueError('incorrect solver: %s' % solver)

        self.solve = self._explicit_solve
        
        self.C = C
        self.time = time
        self.I, self.J, self.ly, self.lz, self.lambda_ = I, J, ly, lz, lambda_
        self.hy = np.double(ly / I)
        self.hz = np.double(lz / J)
        if ht is None:
            self.ht = timestep(min(self.hy, self.hz) / (C * np.sqrt(2)))
        else:
            self.ht = ht
        self.K = int(self.time / self.ht)
        self.u = np.zeros((3, I + 1, J + 1), 'd')

        self._init_bc(0, 0)
        self._init_bc(1, 1)
        self._init_bc(2, 2)
        self._solved = False
        
        if debug:
            self._debug_print()

    def _debug_print(self):
        print "grid: %d x %d" % (self.I, self.J)
        print "time:ht:K = %g:%g:%d" % (self.time, self.ht, self.K)
        print
        print "ly:hy:I = %g:%g:%d" % (self.ly, self.hy, self.I)
        print "lz:hz:J = %g:%g:%d" % (self.lz, self.hz, self.J)
        print
        print "u size (in Kbytes):", ((self.u.size * self.u.itemsize) / 1024.)
        print "extrapolated size (in Mbytes):", (float(self.I * self.J * self.K * self.u.itemsize) / (1024**2))
        print

    def _init_bc(self, k, time):
        for i in xrange(self.u.shape[1]):
            self.u[k, i, 0] = np.sin((np.pi * i) / self.I) * np.sin((2 * np.pi * self.C * self.ht * time) / self.lambda_)

    def _python_step(self):
        g = (self.C**2 * self.ht**2) / self.hy**2
        d = (self.C**2 * self.ht**2) / self.hz**2
        self.u[2, 1:-1, 1:-1] = \
             g * (self.u[1, 2:, 1:-1] + self.u[1, 0:-2, 1:-1]) + \
             d * (self.u[1, 1:-1, 2:] + self.u[1, 1:-1, 0:-2]) + \
             2 * (1 - g - d) * self.u[1, 1:-1, 1:-1] - \
             self.u[0, 1:-1, 1:-1]

    def _blitz_step(self):
        g = float((self.C**2 * self.ht**2) / self.hy**2)
        d = float((self.C**2 * self.ht**2) / self.hz**2)
        u = self.u
        expr = \
        "u[2, 1:-1, 1:-1] = " \
            "g * (u[1, 2:, 1:-1] + u[1, 0:-2, 1:-1]) + " \
             "d * (u[1, 1:-1, 2:] + u[1, 1:-1, 0:-2]) + " \
             "2 * (1 - g - d)  * u[1, 1:-1, 1:-1] - " \
             "u[0, 1:-1, 1:-1]"
        blitz(expr, check_size=0)

    def _inline_step(self):
        g = float((self.C**2 * self.ht**2) / self.hy**2)
        d = float((self.C**2 * self.ht**2) / self.hz**2)
        u = self.u
        I, J, K = self.I, self.J, self.K
        code = """
        for (int i=1; i < I-1; ++i) {
            for (int j = 1; j < J-1; ++j) {
                u(2, i, j) = g * (u(1, i+1, j) + u(1, i-1, j)) +
                             d * (u(1, i, j+1) + u(1, i, j-1)) +
                             2 * (1 - g - d) * u(1, i, j) -
                             u(0, i, j);
            }
        }
        """
        inline(code,
               ['u', 'g', 'd', 'I', 'J', 'K'],
               type_converters=converters.blitz,
               compiler='gcc')
        
    def _explicit_solve(self):
        if not self._solved:
            for k in xrange(2, self.K):
                self._step()
                self.u[0] = self.u[1].copy()
                self.u[1] = self.u[2].copy()
                self._init_bc(2, k + 1)
            self._solved = True
        return self.u[2]


class ImplicitSolver(object):
    '''Represents equation solver with implicit difference scheme
    '''
    
    def __init__(self, I, J, time, ht=None, ly=2e-5, lz=2e-5, lambda_=2e-6, C=3e8, debug=False):

        self.solve = self._implicit_solve
        
        self.C = C
        self.time = time
        self.I, self.J, self.ly, self.lz, self.lambda_ = I, J, ly, lz, lambda_
        self.hy = np.double(ly / I)
        self.hz = np.double(lz / J)
        if ht is None:
            val = self.hy / C
            s = "%g" % val
            l = s.split('e')
            self.ht = np.trunc(np.float64(l[0])) * np.power(10, np.float64(l[1]))
        else:
            self.ht = ht
        self.K = int(self.time / self.ht)
        self.u = np.zeros((3, I + 1, J + 1), 'd')

        self._init_bc(0, 0)
        self._init_bc(1, 1)
        self._init_bc(2, 2)
        self._solved = False
        
        if debug:
            self._debug_print()

    def _debug_print(self):
        print "grid: %d x %d" % (self.I, self.J)
        print "time:ht:K = %g:%g:%d" % (self.time, self.ht, self.K)
        print
        print "ly:hy:I = %g:%g:%d" % (self.ly, self.hy, self.I)
        print "lz:hz:J = %g:%g:%d" % (self.lz, self.hz, self.J)
        print
        print "u size (in Kbytes):", ((self.u.size * self.u.itemsize) / 1024.)
        print "extrapolated size (in Mbytes):", (float(self.I * self.J * self.K * self.u.itemsize) / (1024**2))
        print
        print "\gamma_y =", (self.C**2 * self.ht**2) / self.hy**2
        print "\gamma_z =", (self.C**2 * self.ht**2) / self.hz**2
        print

    def _init_bc(self, k, time):
        for i in xrange(self.u.shape[1]):
            self.u[k, i, 0] = np.sin((np.pi * i) / self.I) * np.sin((2 * np.pi * self.C * self.ht * time) / self.lambda_)

    def _make_system(self):
        '''Формирует матрицу системы для прогонки (ее диагонали).
        '''
        J = self.J
        d = (self.C**2 * self.ht**2) / self.hz**2
        a = np.zeros(J - 2) - d
        b = np.zeros(J - 1) + (1 + 2 * d)
        c = a.copy()
        return a, b, c
    
    def _make_rhs(self, i, k):
        '''Формирует правую часть системы для прогонки для шага i по y
        '''
        g = (self.C**2 * self.ht**2) / self.hy**2
        d = (self.C**2 * self.ht**2) / self.hz**2
        u = self.u
        f = g * (u[1, i + 1, 1:-1] + u[1, i - 1, 1:-1]) + 2 * (1 - g) * u[1, i, 1:-1] - u[0, i, 1:-1]
        f[0] += d * u[2, i, 0]
        f[-1] += d * u[2, i, self.J]
        return f

    def _implicit_solve(self):
        # система всегда одна и та же
        a, b, c = self._make_system()
        if not self._solved:
            for k in xrange(2, self.K):
                self._init_bc(2, k)
                for i in xrange(1, self.I - 1):
                    f = self._make_rhs(i, k)
                    self.u[2, i, 1:-1] = native_sweep(a, b, c, f)
                self.u[0] = self.u[1].copy()
                self.u[1] = self.u[2].copy()
            self._solver = True
        return self.u[2]
