#!/usr/bin/python
#-*- coding: utf-8

import sys
from PyQt4.QtGui import *

from gui import AppForm

def guiMain():
    app = QApplication(sys.argv)
    form = AppForm()
    form.show()
    app.exec_()

if __name__ == '__main__':
    guiMain()

