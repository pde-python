#!/usr/bin/python
# -*- coding: utf-8

import psyco
psyco.full()


from optparse import OptionParser

import numpy as np
import pylab as plt

from grid import ImplicitSolver, ExplicitSolver
from waves import Exm
import const

def prof_main():
    parser = OptionParser()
    parser.add_option("-t", "--time", dest="time", help="Time", metavar="TIME", default="1e-14")
    parser.add_option("-y", dest="ny", help="Y capacity", metavar="I", default="20")
    parser.add_option("-z", dest="nz", help="Z capacity", metavar="J", default="20")
    parser.add_option("-s", "--ht", dest="ht", help="time step", metavar="STEP", default="")
    (options, args) = parser.parse_args()

    time = np.float64(options.time)
    ny = np.int(options.ny)
    nz = np.int(options.nz)
    ht = None
    if (len(options.ht) != 0):
        ht = np.float64(options.ht)

    solver_s = Solver(ny, nz, time, ht, solver='python', debug=True)
    solver_w = Solver(ny, nz, time, ht, solver='blitz', debug=True)
    solver_i = Solver(ny, nz, time, ht, solver='inline', debug=True)

    import time
    t1 = time.time()
    solver_s.solve()
    t2 = time.time()
    solver_w.solve()
    t3 = time.time()
    solver_i.solve()
    t4 = time.time()
    print 'standard:', t2 - t1
    print 'blitz:', t3 - t2
    print 'inline:', t4 - t3
    

def main():
    parser = OptionParser()
    parser.add_option("-t", "--time", dest="time", help="Time", metavar="TIME", default="1e-14")
    parser.add_option("-y", dest="ny", help="Y capacity", metavar="I", default="20")
    parser.add_option("-z", dest="nz", help="Z capacity", metavar="J", default="20")
    parser.add_option("-s", "--ht", dest="ht", help="time step", metavar="STEP", default="")
    (options, args) = parser.parse_args()

    time = np.float64(options.time)
    ny = np.int(options.ny)
    nz = np.int(options.nz)
    ht = None
    if (len(options.ht) != 0):
        ht = np.float64(options.ht)

    # solver = Solver(ny, nz, time, ht, solver='inline', debug=True)
    # u = solver.solve()

    solver = ImplicitSolver(ny, nz, time, ht, debug=True)
    u = solver.solve()

    #solvere = Solver(ny, nz, time, ht, scheme='explicit')
    #ue = solvere.solve()
    
    d = np.arange(0, solver.J + 1)
    z = [solver.hz * j for j in d]
    Ea = [Exm(const.LY/2, z_, time, 1000) for z_ in z]
    En = u[u.shape[0] / 2, :]

    a_line, = plt.plot(z, Ea, lw=1, color="red")
    n_line, = plt.plot(z, En, lw=1, color="green")
    plt.legend((a_line, n_line), (u"Analitycal", u"Implicit scheme"))
    plt.grid(True)
    plt.xlim((min(z), max(z)))
    plt.show()

if __name__ == '__main__':
    main()
