#!/usr/bin/python
#-*- coding: utf-8 -*-

import numpy as np
from scipy.weave import blitz, inline, converters


def sweep(_a, _b, _c, _d):
    n = len(_b)
    a = np.zeros(n)
    a[1:] = _a
    b = _b.copy()
    c = np.zeros(n)
    c[:-1] = _c
    x = _d.copy()

    # Прямой ход
    c[0] /= b[0]
    x[0] /= b[0]
    for i in xrange(1, n):
        denom = 1 / (b[i] - c[i-1] * a[i])
        c[i] *= denom
        x[i] = (x[i] - x[i-1] * a[i]) * denom

    # Обратный ход
    for i in xrange(n - 2, -1, -1):
        x[i] -= c[i] * x[i+1]

    return x

def native_sweep(_a, _b, _c, _d):
    n = len(_b)
    a = np.zeros(n)
    a[1:] = _a
    b = _b.copy()
    c = np.zeros(n)
    c[:-1] = _c
    x = _d.copy()

    code = """
    c(0) /= b(0);
    x(0) /= b(0);
    for (int i=1; i < n; i++) {
        double denom = 1 / (b(i) - c(i - 1) * a(i));
        c(i) *= denom;
        x(i) = (x(i) - x(i - 1) * a(i)) * denom;
    }
    for (int i=n-2; i >= 0; i--) {
        x(i) -= c(i) * x(i+1);
    }
    """
    inline(code,
           ['n', 'a', 'b', 'c', 'x'],
           type_converters=converters.blitz,
           compiler='gcc')
    return x

def main():
    pass


if __name__ == '__main__':
    main()
