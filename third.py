# -*- coding: utf-8 -*

from numpy import *
from const import *

LLY = LY**2
LLZ = LZ**2
LLAMBDA = LAMBDA**2
K = 2 * pi * C / LAMBDA

def setParameters(_ly, _lz, _c, _lambda):
    LY = _ly
    LLY = _ly**2
    LZ = _lz
    LLZ = _lz**2
    C = _c
    LAMBDA = _lambda
    LLAMBDA = _lambda**2
    K = 2 * pi * C / LAMBDA

def w(m):
    return pi * C * sqrt(1/LLY + m**2 / LLZ)

def psi(y, z, t):
    return (LZ - z) / LZ * sin(pi * y / LY) * sin(K * t)

def D(m):
    val = 2/(m * pi)
    val *= LLZ * (4 * LLY - LLAMBDA)
    val /= (LLZ * LLAMBDA + m**2 * LLY * LLAMBDA - 4 * LLY * LLZ)
    return val

def gamma(m, t):
    val = D(m) * sin(K*t)
    val -= K / w(m) * (2 / (m * pi) + D(m)) * sin(w(m) * t)
    return val

def um(y, z, t, m):
    return gamma(m, t) * sin(pi * y / LY) * sin(m * pi * z / LZ)

def Um(y, z, t, _M):
    v = 0.0
    for m in xrange(1, _M + 1):
        v += um(y, z, t, m)
    return v

def U(y, z, t):
    return Um(y, z, t, M)

def Ex(y, z, t):
    return U(y, z, t) + psi(y, z, t)

def Exm(y, z, t, _M):
    return Um(y, z, t, _M) + psi(y, z, t)

def remainder_length(eps):
    G = sqrt(4 * LY**2 - LAMBDA**2)/LAMBDA * (LZ / LY)
    M = LAMBDA * G**2 + 2 * LZ
    M += sqrt((LAMBDA * G**2 + 2 * LZ)**2 + 8 * pi * LAMBDA * LZ * eps * G**2)
    M /= (pi * LAMBDA * eps)
    if M < 20.0:
        M = 20
    return int(round(M + 0.49))

def analyze_remainder(M, eps, y, z, t):
    last = 0
    prev_sum = 0
    curr_sum = 0
    for m in xrange(1, M+1):
        curr_sum += um(y, z, t, m)
        if (abs(curr_sum - prev_sum) > eps):
            last = m
            prev_sum = curr_sum
    print prev_sum
    print curr_sum
    return last
